<?php

namespace Indensive\RoleAndPermission\Providers;

use Illuminate\Support\ServiceProvider;
use Indensive\RoleAndPermission\SingletonContract;
use Indensive\RoleAndPermission\Singleton;

class RoleAndPermissionProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton(SingletonContract::class, function ($app) {
            return new Singleton();
        });
    }

    public function boot()
    {
        if ($this->app->runningInConsole()) {
            $this->publishes([
                __DIR__ . '/../config/role_and_permission.php' => config_path('role_and_permission.php')
            ]);
       }
    }
}