<?php

namespace Indensive\RoleAndPermission;

class Singleton implements SingletonContract
{
    private $roles;
    private $permissions;    

    public function __construct()
    {
        $config = config('role_and_permission');
        $rolesConfig = config($config['use_config']);

        $user = auth()->user();
        if ($user) {
            foreach($rolesConfig[$config['role_structure']] as $role => $item) {
                $this->roles[$role] = $user->hasRole($role);
            }
    
            foreach($rolesConfig[$config['permission_structure']] as $permission => $item) {
                foreach (explode(',', $item) as $concrete) {
                    $result = "{$rolesConfig[$config['permissions_map']][$concrete]}_$permission";
                    $this->permissions[$result] = $user->hasPermissionTo($result);
                }
            }
        }
    }

    public function isRole($role): bool
    {        
        return isset($this->roles[$role]) ? $this->roles[$role] : false;
    }

    public function isPermission($permission): bool
    {
        return isset($this->permissions[$permission]) ? $this->permissions[$permission] : false;
    }
}