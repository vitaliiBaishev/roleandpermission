<?php

namespace Indensive\RoleAndPermission;

interface SingletonContract
{
    public function isRole(string $role): bool;
    public function isPermission(string $permission): bool;
}