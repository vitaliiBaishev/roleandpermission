<?php
return [
    // which config file to use
    'use_config' => 'roles',

    // 'role_structure' => [
    //  'admin',
    //  'user'
    // ]
    'role_structure'        => 'role_structure',

    // 'permissions_map' => [
    //     'c' => 'create',
    //     'r' => 'read',
    //     'u' => 'update',
    //     'd' => 'delete'
    // ]
    'permissions_map'       => 'permissions_map',
    
    // 'permission_structure' => [
    //     'admin' => 'c,r,u,d',
    //     'user' => 'r,u',
    // ]
    'permission_structure'  => 'permission_structure',
];