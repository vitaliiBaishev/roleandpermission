<?php

namespace Indensive\RoleAndPermission\Traits;

use Indensive\RoleAndPermission\SingletonContract;

trait RoleAndPermission
{    
    public function isRole(string $role)
    {
        return app(SingletonContract::class)->isRole($role);
    }
    
    public function isPermission(string $permissions)
    {
        return app(SingletonContract::class)->isPermission($permissions);
    }
}