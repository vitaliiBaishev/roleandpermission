Надстройка над пакетом spatie/laravel-permission сокращающая время проверки большого обьёма данных

Установка: 
добавить провайдер в конфиг app
Indensive\RoleAndPermission\Providers\RoleAndPermissionProvider::class,

опубликовать конфиг
php artisan vendor:publish --provider="Indensive\RoleAndPermission\Providers\RoleAndPermissionProvider\"

добавить в модель User трейт RoleAndPermission

Доступные методы:
isRole(string $role), isPermission($permission)
