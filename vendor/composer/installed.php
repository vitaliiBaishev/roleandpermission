<?php return array(
    'root' => array(
        'name' => 'indensive/role-and-permission',
        'pretty_version' => 'dev-master',
        'version' => 'dev-master',
        'reference' => '63c9efa589a32a5de69217e84aca7569b8dff32e',
        'type' => 'library',
        'install_path' => __DIR__ . '/../../',
        'aliases' => array(
            0 => '1.1.x-dev',
        ),
        'dev' => true,
    ),
    'versions' => array(
        'indensive/role-and-permission' => array(
            'pretty_version' => 'dev-master',
            'version' => 'dev-master',
            'reference' => '63c9efa589a32a5de69217e84aca7569b8dff32e',
            'type' => 'library',
            'install_path' => __DIR__ . '/../../',
            'aliases' => array(
                0 => '1.1.x-dev',
            ),
            'dev_requirement' => false,
        ),
    ),
);
